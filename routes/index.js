var express = require('express');
var router = express.Router();

const pages = [
  {path: 'about', title: 'About',},
  {path: 'projects', title: 'Projects',},
  {path: 'contact', title: 'Contact',},
]

// Redirect from root
router.get('/', function(req, res, next) {
  res.redirect("/about");
});

// Get pages
for(let pp in pages) {
  router.get('/'+pages[pp].path, function(req, res, next) {
    res.render('index', { pages, activeTab: pp });
  });
}

// 404 ERROR page
router.get('/*', function(req, res, next) {
  var err = new Error('Not Found');
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(404);
  res.render('error', { pages });
});

module.exports = router;
