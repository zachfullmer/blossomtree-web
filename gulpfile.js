const gulp = require("gulp");
const sass = require('gulp-sass');
const watchSass = require("gulp-watch-sass");
const exec = require('child_process').exec;

gulp.task("sass", function () {
    // compile
    gulp.src("./public/**/*.{scss,css}")
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest("./public"));
    // watch
    return watchSass([
        "./public/**/*.{scss,css}"
      ])
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest("./public"));
});

gulp.task("node",function(cb) {
    exec("node ./bin/www", function (err, stdout, stderr) {
      console.log(stdout);
      console.log(stderr);
      cb(err);
    });
});

gulp.task("default",["sass","node"]);